#!/usr/bin/python3
## Google translate with python requests
import requests
import json
url = "http://translate.googleapis.com/translate_a/single?client=gtx&sl={}&tl={}&dt=t&q={}"
sl = "auto"
tl = "en"
def set_source(name="auto"):
    global sl
    sl = name

def set_target(name="tr"):
    global tl
    tl = name

def translate(word):
    response = requests.get(url.format(sl,tl,word))
    ctx = response.content.decode("utf-8")
    data = json.loads(ctx)
    obj = __translation(word)
    obj.output = data[0][0][0]
    obj.language = data[2]
    return obj

class __translation:
    def __init__(self,source):
        self.source = source
        self.output = ""
        self.language = ""


if __name__ == "__main__":
    import sys
    word = ""
    for arg in sys.argv[1:]:
        word += arg + " "
    sys.stdout.write(translate(word).output+"\n")
    sys.stdout.flush()

import os
import sys
import time
import subprocess
from pathlib import Path
files=[]
dirs=[]
single=False
recursive = True
hidden=False
norun=False
args=[]
for arg in sys.argv[1:]:
    if "--help" == arg:
        print("Search file recursively and run command")
        print()
        print("Usage: oto.py [args]")
        print("  %FILE%          : filename")
        print("  %TIME%          : current date")
        print("  %SIZE%          : file size")
        print("  %xxxx%          : run in python console and get output")
        print("  --single        : do not search any file and execute a single line")
        print("  --no-recursive  : disable recursive search")
        print("  --hidden        : search hidden files and directories")
        print("  --no-run        : do not run command and print commands")
        exit(1)
    elif "--single" == arg:
        single=True
    elif "--no-recursive" == arg:
        recursive = False
    elif "--hidden" == arg:
        hidden=True
    elif "--no-run" == arg:
        norun=True
    else:
        args.append(arg)

def run(cmd):
    if not norun:
        sys.stderr.write("\x1b[32mRunnig:\x1b[0m")
    sys.stderr.write(cmd+"\n")
    if not norun:
        i=os.system(cmd)
        if  i != 0:
            sys.stderr.write("\x1b[31mFailed to run command\x1b[0m\n")

def find_recursive(dir=".",type="file"):
    curdir=os.getcwd()
    os.chdir(dir)
    for i in os.listdir():
        if i.startswith(".") and not hidden:
            continue
        if os.path.isdir(i):
            if recursive:
                find_recursive(i)
            dirs.append(i)
        else:
            files.append(dir+"/"+i)
    os.chdir(curdir)

find_recursive()
def process(file=""):
    ret=""
    for arg in args:
        c=arg.replace("%FILE%",file)
        c=c.replace("%TIME%",str(time.time()))
        if os.path.isfile(file):
            c=c.replace("%SIZE%",str(Path(file).stat().st_size))
        else:
            c=c.replace("%SIZE%","0")
        c=c.replace("%DIR%",os.path.dirname(file))
        c=c.replace("%NAME%",os.path.basename(file))
        
        if len(c)>2 and c[0] == '%' and c[-1] == '%':
            ret+=subprocess.getoutput("python3 -c \""+c[1:-1]+"\"")+" "
        else:
            ret+=c+" "
    return ret

if single:
    run(process("."))
else:
    for file in files:
        run(process(os.path.realpath(file)))

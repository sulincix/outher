import os, gi, sys, time	

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk, Gtk
from PIL import Image

sampling = 100
interval = 60

if os.getuid() != 0:
    print("You must be root!")
    sys.exit(1)

def get_image_light(im):
    width, height = im.size
    c = 0
    p = 0
    for w in range(int(width/sampling)):
        for h in range(int(height/sampling)):
            rx,gx,bx = pix[w*sampling,h*sampling]
            c+=rx+bx+gx
            p += 1

    percent = (100*c)/(255*p)
    print("Percent: {}".format(percent))
    return percent

def set_light(percent):
    for dir in os.listdir("/sys/class/backlight"):
        max = int(open("/sys/class/backlight/{}/max_brightness".format(dir),"r").read())
        value = int((max * percent) / 100)
        node = open("/sys/class/backlight/{}/brightness".format(dir),"w")
        node.write("{}".format(value))


while True:
    os.system("mkdir -p /run/autobg && chown root -R /run/autobg && chmod 700 -R /run/autobg")
    i = os.system("gst-launch-1.0 v4l2src num-buffers=1 ! jpegenc ! filesink location=/run/autobg/.webcam.jpg &>/dev/null")
    if i != 0:
        sys.exit(0)
    im = Image.open("/run/autobg/.webcam.jpg")
    pix = im.load()
    os.unlink("/run/autobg/.webcam.jpg")
    percent = get_image_light(im)
    set_light(percent)
    time.sleep(interval)

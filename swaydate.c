#include <stdio.h>
#include <time.h>
#include <unistd.h>

static time_t rawtime;
static   struct tm * timeinfo;
static  char buffer [80];

int main (int argc, char** argv) {
  (void)argc; (void)argv;
  while(1){
      time (&rawtime);
      timeinfo = localtime (&rawtime);

      strftime (buffer,80,"%Y-%m-%d %X",timeinfo);
      fprintf (stdout, "%s\n",buffer);
      fflush(stdout);
      sleep(1);
  }
  return 0;
}